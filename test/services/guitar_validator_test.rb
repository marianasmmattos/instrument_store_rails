# frozen_string_literal: true

require 'test_helper'

class GuitarValidatorTest < ActionDispatch::IntegrationTest
  setup do
    stub_request(:get, "https://calm-beach-16451.herokuapp.com/search?").with(query: { "model" => "Jackson", "year" => 2020 })
    .to_return(
      :status => 200,
      :body => JSON.generate(
        {
          "isStolen": true
        }),
      :headers => {"Content-Type"=> "application/json"}
    )

    stub_request(:get, "https://calm-beach-16451.herokuapp.com/search?").with(query: { "model" => "MyModel", "year" => 2022 })
    .to_return(
      :status => 200,
      :body => JSON.generate(
        {
          "isStolen": false
        }),
      :headers => {"Content-Type"=> "application/json"}
    )
  end

  test 'when params are valid and guitar is stolen should return false' do
    params = { model: "Jackson", year: 2020 }
    subject = GuitarValidator.valid?(params)
    assert_equal false, subject
  end

  test 'when params are valid and guitar is not stolen should return true' do
    params = { model: "MyModel", year: 2022 }
    subject = GuitarValidator.valid?(params)
    assert_equal true, subject
  end
end

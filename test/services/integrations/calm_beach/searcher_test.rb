# frozen_string_literal: true

require 'test_helper'

class SearcherTest < ActionDispatch::IntegrationTest
  setup do
    stub_request(:get, "https://calm-beach-16451.herokuapp.com/search?").with(query: { "model" => "Jackson", "year" => 2020 })
    .to_return(
      :status => 200,
      :body => JSON.generate(
        {
          "isStolen": true
        }),
      :headers => {"Content-Type"=> "application/json"}
    )

    stub_request(:get, "https://calm-beach-16451.herokuapp.com/search?").with(query: { "model" => "MyModel", "year" => 2022 })
    .to_return(
      :status => 200,
      :body => JSON.generate(
        {
          "isStolen": false
        }),
      :headers => {"Content-Type"=> "application/json"}
    )
  end

  test 'when params are valid and guitar is stolen should return hash with true symbol value' do
    params = { model: "Jackson", year: 2020 }
    subject = ::Integrations::CalmBeach::Searcher.new(params).call
    assert_equal true, subject[:isStolen]
  end

  test 'when params are valid and guitar is not stolen should return hash with false symbol value' do
    params = { model: "MyModel", year: 2022 }
    subject = ::Integrations::CalmBeach::Searcher.new(params).call
    assert_equal false, subject[:isStolen]
  end
end

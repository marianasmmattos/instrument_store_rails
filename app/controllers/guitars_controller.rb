# frozen_string_literal: true

class GuitarsController < ApplicationController
  before_action :check_stolen_guitar, only: :create

  def index
    @guitars = Guitar.all
  end

  def show
    @guitar = Guitar.find(params[:id])
  end

  def new
    @guitar = Guitar.new
  end

  def create
    @guitar = Guitar.new(guitar_params.merge({ process: true }))

    respond_to do |format|
      if @guitar.save
        format.html do
          redirect_to guitars_url,
                      notice: 'Guitar was successfully created.'
        end
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    guitar = Guitar.find(params[:id])
    guitar.destroy

    respond_to do |format|
      format.html { redirect_to guitars_url, notice: 'Guitar was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def guitar_params
    params.require(:guitar).permit(:model, :year, :is_available)
  end

  def check_stolen_guitar
    ValidateGuitarWorker.perform_async(guitar_params[:model], guitar_params[:year])
  end
end
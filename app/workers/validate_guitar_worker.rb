class ValidateGuitarWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(model, year)
    guitar_params = { model: model, year: year }
    guitar_valid = GuitarValidator.valid?(guitar_params)
    current_guitar = Guitar.find_by(guitar_params)

    if guitar_valid
      current_guitar.process = nil
      current_guitar.save
      return
    end

    current_guitar.stolen = !guitar_valid
    current_guitar.process = nil
    current_guitar.save
  end
end

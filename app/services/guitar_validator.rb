class GuitarValidator
  def self.valid?(params)
    new(params).perform
  end

  def initialize(params)
    @params = params
  end

  def perform
    !guitar[:isStolen]
  end

  private

  attr_reader :params

  def guitar
    ::Integrations::CalmBeach::Searcher.new(params).call
  end
end

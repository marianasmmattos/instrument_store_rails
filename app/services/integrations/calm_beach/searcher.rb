module Integrations
  module CalmBeach
    class Searcher
      def initialize(params)
        @params = params
      end

      def call
        response = ::Integrations::CalmBeach::Fetcher.new(normalized_params).call
        response.transform_keys(&:to_sym)
      end

      private

      attr_reader :params

      def normalized_params
        hash = params
        hash.deep_stringify_keys
      end
    end
  end
end

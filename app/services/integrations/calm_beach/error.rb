module Integrations
  module CalmBeach
    class Error < StandardError
      def self.call(request, search_params)
        new(request, search_params).perform
      end

      def initialize(request, search_params)
        @request = request
        @search_params = search_params
      end

      def perform
        "CalmBeach request responded with a #{request.code} status for #{search_params}" 
      end

      private

      attr_reader :request, :search_params
    end
  end
end

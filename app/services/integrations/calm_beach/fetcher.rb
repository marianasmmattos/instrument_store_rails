module Integrations
  module CalmBeach
    class Fetcher
      include HTTParty

      BASE_URL = ENV["CALM_BEACH_URL"]

      def initialize(search_params = {})
        @search_params = search_params
      end

      def call
        response = HTTParty.get(url, **params)

        return JSON.parse(response.body) if response.ok?

        raise ::Integrations::CalmBeach::Error.call(response, search_params)
      end

      private

      attr_reader :search_params

      def url
        return search_url if search_params.present?

        BASE_URL
      end

      def search_url
        [BASE_URL, :search?].join("/")
      end

      def params
        {
          query: search_params,
          headers: { "X-ACCESS-TOKEN" => ENV["CALM_BEACH_TOKEN"] }
        }
      end
    end
  end
end

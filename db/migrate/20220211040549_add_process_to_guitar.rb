class AddProcessToGuitar < ActiveRecord::Migration[7.0]
  def change
    add_column :guitars, :process, :boolean
  end
end
